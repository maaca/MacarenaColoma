
public class Electrodomestico {


		 double preciobase=100;
		 String color = "blanco";
		 char consumo_energtico = 'F';
		 double peso=5;
		 String colores[] = {"blanco", "negro", "rojo", "azul", "gris"};
		 char consumos[] = {'A', 'B', 'C', 'D', 'E', 'F'};
		 

		 
		protected double getPreciobase() {
			return preciobase;
		}

		protected void setPreciobase(double preciobase) {
			this.preciobase = preciobase;
		}

		protected String getColor() {
			return color;
		}

		protected void setColor(String color) {
			this.color = color;
		}

		protected char getConsumo_energtico() {
			return consumo_energtico;
		}

		protected void setConsumo_energtico(char consumo_energtico) {
			this.consumo_energtico = consumo_energtico;
		}

		protected double getPeso() {
			return peso;
		}

		protected void setPeso(double peso) {
			this.peso = peso;
		}

		protected Electrodomestico() {
			this.preciobase=100;
			this.color = "blanco";
			this.consumo_energtico = 'F';
			this.peso=5;
		}
		
		protected Electrodomestico(double precio, double peso) {
			this.preciobase=precio;
			this.color = "blanco";
			this.consumo_energtico = 'F';
			this.peso=peso;
		}
		
		protected Electrodomestico(double precio,String color, char consumo_energtico, double peso) {
			this.preciobase=precio;
			this.color = color;
			this.consumo_energtico = consumo_energtico;
			this.peso=peso;
		}
		
		
		 
	}


