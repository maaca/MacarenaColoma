
public class Serie {

	String titulo;
	int numTemporadas=3;
	boolean entregado=false;
	String genero;
	String creador;
	
	protected String getTitulo() {
		return titulo;
	}
	protected void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	protected int getNumTemporadas() {
		return numTemporadas;
	}
	protected void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}
	protected boolean isEntregado() {
		return entregado;
	}
	protected void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}
	protected String getGenero() {
		return genero;
	}
	protected void setGenero(String genero) {
		this.genero = genero;
	}
	protected String getCreador() {
		return creador;
	}
	protected void setCreador(String creador) {
		this.creador = creador;
	}
	
	protected Serie() {
		this.titulo="";
		this.genero="";
		this.creador="";
	}
	
	protected Serie(String titulo, String creador) {
		this.titulo=titulo;
		this.genero="";
		this.creador=creador;
	}
	
	protected Serie(String titulo, String creador, String genero) {
		this.titulo=titulo;
		this.genero=genero;
		this.creador=creador;
	}
	



}
