
public class Password {


	private int longitud=8;
	private String contrasea;
	
	protected Password() {
		this.contrasea="";
	}
	
	protected Password(int longitud) {
		this.longitud=longitud;
		contrasea=getPass();
	}

	protected int getLongitud() {
		return longitud;
	}

	protected void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	protected String getContrasea() {
		return contrasea;
	}

	protected void setContrasea(String contrasea) {
		this.contrasea = contrasea;
	}
	
	  public String getPass (){
		  
	        String pass="";
	        
	        for (int i=0;i<longitud;i++){
	            
	            int eleccion=((int)Math.floor(Math.random()*3+1));
	  
	            if (eleccion==1){
	                char minus=(char)((int)Math.floor(Math.random()*(123-97)+97));
	                	pass+=minus;
	            }else{
	                if(eleccion==2){
	                    char mayus=(char)((int)Math.floor(Math.random()*(91-65)+65));
	                    pass+=mayus;
	                }else{
	                    char nums=(char)((int)Math.floor(Math.random()*(58-48)+48));
	                    pass+=nums;
	                }
	            }
	        }
	        return pass;
	    }
}



